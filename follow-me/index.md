---
title: Compose Concepts
---

Jetpack Compose is the recommended User Interface (UI) framework for use when developing Android applications. It greatly simplifies creation of UIs and management of state used by those UIs. It can be deceptively simple at times, and if you don't understand some of what's going on behind the scenes, it'll seem like magic, and possibly result in some tricky bugs to resolve.

In this module, we'll start with the basic concepts, and drill in for some deeper discussion on how Compose uses state to determine when the UI should update.

The "big idea" of Compose is fairly simple:

   * Compose is declarative. You *declare* your user interface in "composable functions", rather than *create* it. Think of it as telling the framework what you want, rather than how to create it.
   * Compose is unidirectional. You pass data *in*, and get events *out*. Many user interface frameworks encourage modification of data inside the UI elements you create. This can make it difficult to test data flow and reason about how the application behaves. In Compose, you:
      * Pass data to composable functions that declare the UI.
      * Pass callback "event" functions to those composable functions.
      * The composable function calls the event functions to tell the caller "something happened".
