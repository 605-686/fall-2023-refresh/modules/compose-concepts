---
title: Density
---

Android runs on many devices, each of which can have different screen densities. Some might only have a hundred or so pixels per inch, others thousands. We call this "screen density", measured in "dots per inch", or DPI.

If you specify sizes in pixels, the resulting element or drawing will appear different sizes on different screens. The lower the density, the bigger the pixels. Pixel-based elements will appear larger on lower-density screens, and smaller as the density rises.

Android abstracts sizing by using "density independent pixels"; DIP or DP for short. Each DP appears the same physical size as a real pixel on a 160 DPI screen. (In other words, a DP is 1/160 inch wide/high).

If you specify sizes using DP, the system will perform automatic scaling to preserve the same physical size on all devices. This is particularly useful for margin and tap-regios sizes (such as button), ensuring a consistent look and usable tapping on all devices. Other elements in the UI, such as images in a photo app, can use relative sizing, taking advantage of larger screen sizes for better viewing.

![Absolute Pixels vs Density-Independent Pixels](images/dpi.png)

In this image we see four emulators running at different screen densities, lowest density on the left. Note how `200dip` (or more commonly, `200dp`) looks the same size on different density screens, whereas `200px` varies. The second emulator is sized at 160DPI, the size that defines density-independent pixels.

We'll typically use sizes such as `48dp` for buttons, as that's about the size of a typical thumb.